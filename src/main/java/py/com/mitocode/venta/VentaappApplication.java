package py.com.mitocode.venta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentaappApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentaappApplication.class, args);
	}
}
