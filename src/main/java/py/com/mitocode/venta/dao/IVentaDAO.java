package py.com.mitocode.venta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import py.com.mitocode.venta.model.Venta;

@Repository
public interface IVentaDAO extends JpaRepository<Venta, Integer>{

}
