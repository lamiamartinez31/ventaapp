package py.com.mitocode.venta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import py.com.mitocode.venta.model.Producto;

@Repository
public interface IProductoDAO extends JpaRepository<Producto, Integer> {

}
