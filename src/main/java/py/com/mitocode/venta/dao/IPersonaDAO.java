package py.com.mitocode.venta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import py.com.mitocode.venta.model.Persona;

@Repository
public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
