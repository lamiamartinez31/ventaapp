package py.com.mitocode.venta.service;

import java.util.List;

import py.com.mitocode.venta.model.Venta;

public interface IVentaService {

	Venta registrar(Venta venta);

	void modificar(Venta venta);

	void eliminar(int idVenta);

	Venta listarId(int idVenta);

	List<Venta> listar();
}
