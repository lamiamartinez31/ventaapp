package py.com.mitocode.venta.service;

import java.util.List;

import py.com.mitocode.venta.model.Producto;

public interface IProductoService {

	void registrar(Producto producto);

	void modificar(Producto producto);

	void eliminar(int idMedico);

	Producto listarId(int idProducto);

	List<Producto> listar();
}
