package py.com.mitocode.venta.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.mitocode.venta.dao.IVentaDAO;
import py.com.mitocode.venta.model.Venta;
import py.com.mitocode.venta.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private IVentaDAO dao;

	@Override
	public Venta registrar(Venta venta) {		
		//Esto es java7
		/*for(DetalleConsulta det : consulta.getDetalleConsulta()) {
			det.setConsulta(consulta);
		}*/
		//java 8
		venta.getDetalleVenta().forEach(x -> x.setVenta(venta));		
		return dao.save(venta);
	}

	@Override
	public void modificar(Venta venta) {
		dao.save(venta);
	}

	@Override
	public void eliminar(int idVenta) {
		dao.delete(idVenta);
	}

	@Override
	public Venta listarId(int idVenta) {
		return dao.findOne(idVenta);
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}

}
