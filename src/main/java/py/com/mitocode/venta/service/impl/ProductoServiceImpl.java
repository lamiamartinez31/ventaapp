package py.com.mitocode.venta.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.mitocode.venta.dao.IProductoDAO;
import py.com.mitocode.venta.model.Producto;
import py.com.mitocode.venta.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoDAO dao;

	@Override
	public void registrar(Producto producto) {
		dao.save(producto);
	}

	@Override
	public void modificar(Producto producto) {
		dao.save(producto);
	}

	@Override
	public void eliminar(int idProducto) {
		dao.delete(idProducto);
	}

	@Override
	public Producto listarId(int idProducto) {
		return dao.findOne(idProducto);
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

}
